import KeycloakApi from '../keycloak/KeycloakApi';
import api from '../index';

jest.mock('../utils/bearerToken');
jest.mock('../utils/parseToken');
jest.mock('../keycloak/KeycloakApi');

test('test1', () => {
  api();

  expect(KeycloakApi).toBeCalled();
});
