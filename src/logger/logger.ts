/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { ILogger, DummyLogger } from '@s2m/logger';
import { ELogErrorCode } from './ELogErrorCode';
import { ELogModule } from './ELogModule';

interface ICumulativeLogger {
  manager: ILogger<ELogModule, ELogErrorCode>;
}

let cumulativeLogger: ICumulativeLogger;

let parentLogger: DummyLogger<ELogModule, ELogErrorCode> = new DummyLogger();

const initLoggers = (logger: ILogger<ELogModule, ELogErrorCode>) => {
  cumulativeLogger = {
    manager: logger.createBoundChild(ELogModule.ResourceManagement),
  };
};

initLoggers(parentLogger);

export const registerLogger = (logger: ILogger<ELogModule, ELogErrorCode>): void => {
  parentLogger = logger;

  initLoggers(parentLogger);
};

export const getLogger = (): ICumulativeLogger => {
  return cumulativeLogger;
};
