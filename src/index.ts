import KeycloakApi from './keycloak/KeycloakApi';
import bearerToken from './utils/bearerToken';
import parseToken from './utils/parseToken';
import { registerLogger } from './logger/logger';

export type TConfig = {
  baseUrl?: string;
  realmName?: string;
};

export enum GrantType {
  REFRESH_TOKEN = 'refresh_token',
  PASSWORD = 'password',
  CLIENT_CREDENTIALS = 'client_credentials',
}

export type TCred = {
  grantType: GrantType;
  clientId: string;
  clientSecret: string;
};

export type TUserInfo = {
  displayName: string;
  email: string;
  id: string;
  realmName: string;
};

export type TToken = {
  accessToken: string;
  refreshToken: string;
  expiresIn: number;
  refreshExpiresIn: number;
};

export type TGroupedUsers = {
  viewers?: Array<string>;
  owners?: Array<string>;
};

export enum GroupName {
  viewers = 'viewers',
  owners = 'owners',
}

export type TResourceDesc = {
  name: string;
  groups: GroupName[];
};

export type TResourceGroupMap = {
  [key: string]: {
    groups: GroupName[];
  };
};

export enum Scope {
  READ = 'read',
  DELETE = 'delete',
}

export { bearerToken, parseToken, registerLogger };

export interface IResourceApi {
  assocResourceWithUsers(cred: TCred, recordId: string, groupedUsers: TGroupedUsers): Promise<void>;

  dissocResourceWithAllUsers(cred: TCred, recordId: string): Promise<void>;

  hasReadPermissions(cred: TCred, recordId: string, token: string): Promise<boolean>;

  hasDeletePermissions(cred: TCred, recordId: string, token: string): Promise<boolean>;

  validateToken(cred: TCred, token: string): Promise<boolean>;

  fetchUserInfo(token: string): Promise<TUserInfo>;

  fetchToken(cred: TCred, userName: string, password: string): Promise<TToken>;

  refreshToken(cred: TCred, refreshToken: string): Promise<TToken>;

  fetchResourceNamesByUserId(cred: TCred, userId: string): Promise<TResourceDesc[]>;

  fetchUserIdsByResourceId(cred: TCred, recordId: string): Promise<string[]>;
}

export default (config?: TConfig): IResourceApi => new KeycloakApi(config);
