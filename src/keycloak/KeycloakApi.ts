import { TCred, IResourceApi, TToken, TUserInfo, TConfig, TGroupedUsers, Scope, GroupName, TResourceDesc } from '../index';
import CustomKeycloakAdminClient from './CustomKeycloakAdminClient';
import { StatusCodes } from 'http-status-codes';
import { AxiosError } from 'axios';
import parseToken from '../utils/parseToken';
import GroupRepresentation from 'keycloak-admin/lib/defs/groupRepresentation';
import uniq from 'lodash/uniq';
import getResourceGroupName from './utils/getResourceGroupName';
import getResourceNameByGroup from './utils/getResourceNameByGroup';
import isResourceGroups from './utils/isResourceGroups';
import toResourceGroupMap from './utils/toResourceGroupMap';
import { getLogger } from '../logger/logger';

const continueIfStatusCodeIsConflict = (error: AxiosError): void => {
  if (error?.response?.status !== StatusCodes.CONFLICT) {
    throw error;
  }
};

const createRecordingResourceDescriptionIfNeed = async (restClient: CustomKeycloakAdminClient, resourceName: string): Promise<string> => {
  try {
    return await restClient.protection.createRecordingResourceDescription(resourceName);
  } catch (e) {
    continueIfStatusCodeIsConflict(e);
  }

  return restClient.protection.getResourceIdByResourceName(resourceName);
};

const createGroupIfNeed = async (
  restClient: CustomKeycloakAdminClient,
  { groupName }: { groupName: string }
): Promise<GroupRepresentation> => {
  if (groupName.includes('/')) {
    throw new Error('Incorrect group name.');
  }

  try {
    await restClient.groups.create({
      name: groupName,
    });

    getLogger().manager.debug('Created group with groupName', { groupName });
  } catch (e) {
    continueIfStatusCodeIsConflict(e);
  }

  const foundGroups = await restClient.groups.find({
    search: groupName,
  });
  const groups = foundGroups.filter((g) => g.name === groupName);

  if (groups.length === 0) {
    throw new Error(`Cannot create or find group with name ${groupName}`);
  }

  return groups[0];
};

const createResourceGroupPolicyIfNeed = async (
  restClient: CustomKeycloakAdminClient,
  { resourceId, groupName, scopes }: { resourceId: string; groupName: string; scopes: Array<Scope> }
): Promise<void> => {
  try {
    const policyName = `Allow ${scopes.join(',')} ${groupName}`;

    await restClient.protection.createUmaPolicy({
      resourceId,
      name: policyName,
      groups: [groupName],
      scopes,
    });

    getLogger().manager.debug('Created policy for resource', { policyName, resourceId });
  } catch (e) {
    continueIfStatusCodeIsConflict(e);
  }
};

const createSubGroupWithPolicy = async (
  restClient: CustomKeycloakAdminClient,
  {
    userIds = [],
    resourceId,
    groupName,
    parentGroup,
    scopes,
  }: {
    userIds: Array<string>;
    resourceId: string;
    parentGroup: GroupRepresentation;
    groupName: GroupName;
    scopes: Array<Scope>;
  }
) => {
  let subGroup = parentGroup.subGroups.find((subGroup) => subGroup.name === groupName);

  if (!subGroup) {
    const subGroupId = (await restClient.groups.setOrCreateChild({ id: parentGroup.id }, { name: groupName })).id;

    getLogger().manager.debug('Added subgroup to parent', {
      groupName,
      parentGroupId: parentGroup.id,
    });

    subGroup = await restClient.groups.findOne({ id: subGroupId });
  }

  await createResourceGroupPolicyIfNeed(restClient, {
    resourceId,
    groupName: subGroup.path,
    scopes,
  });

  for (const id of userIds) {
    await restClient.users.addToGroup({ id, groupId: subGroup.id });
  }
};

export default class KeycloakApi implements IResourceApi {
  constructor(private config?: TConfig) {}

  private async getRestClient(cred: TCred): Promise<CustomKeycloakAdminClient> {
    const patKcAdminClient = new CustomKeycloakAdminClient(this.config);
    const pat = await patKcAdminClient.openIdConnect.permissionAccessToken(cred);

    patKcAdminClient.setAccessToken(pat.access_token);

    return patKcAdminClient;
  }

  public async assocResourceWithUsers(cred: TCred, recordId: string, groupedUsers: TGroupedUsers): Promise<void> {
    const resourceName = getResourceGroupName(recordId);
    const restClient = await this.getRestClient(cred);
    const resourceId = await createRecordingResourceDescriptionIfNeed(restClient, resourceName);
    const parentGroup = await createGroupIfNeed(restClient, {
      groupName: resourceName,
    });

    await createSubGroupWithPolicy(restClient, {
      userIds: groupedUsers.viewers,
      resourceId,
      groupName: GroupName.viewers,
      parentGroup,
      scopes: [Scope.READ],
    });

    await createSubGroupWithPolicy(restClient, {
      userIds: groupedUsers.owners,
      resourceId,
      groupName: GroupName.owners,
      parentGroup,
      scopes: [Scope.DELETE, Scope.READ],
    });
  }

  public async dissocResourceWithAllUsers(cred: TCred, recordId: string): Promise<void> {
    const resourceName = getResourceGroupName(recordId);
    const restClient = await this.getRestClient(cred);
    const resourceId = await restClient.protection.getResourceIdByResourceName(resourceName);
    const foundGroups = await restClient.groups.find({
      search: resourceName,
    });

    if (foundGroups.length !== 1) {
      throw new Error(`State of policy-groups inconsistent for resource ${resourceName}.`);
    }

    getLogger().manager.debug('Delete resource description by recordId and resourceId', {
      recordId,
      resourceId,
    });

    await restClient.protection.deleteRecordingResourceDescription({
      resourceId,
    });

    const groupId = foundGroups[0].id;

    getLogger().manager.debug('Delete group by recordId, resourceId and groupId', {
      recordId,
      resourceId,
      groupId,
    });

    await restClient.groups.del({
      id: groupId,
    });
  }

  public async hasReadPermissions(cred: TCred, recordId: string, token: string): Promise<boolean> {
    const resourceName = getResourceGroupName(recordId);
    const restClient = await this.getRestClient(cred);
    const resourceId = await restClient.protection.getResourceIdByResourceName(resourceName);
    const userClient = CustomKeycloakAdminClient.createFromToken(token);

    getLogger().manager.debug('Check read permission for recordId and resourceId', {
      recordId,
      resourceId,
    });

    return await userClient.openIdConnect.hasPermission({
      audience: cred.clientId,
      permission: `${resourceId}#${Scope.READ}`,
    });
  }

  public async hasDeletePermissions(cred: TCred, recordId: string, token: string): Promise<boolean> {
    const resourceName = getResourceGroupName(recordId);
    const restClient = await this.getRestClient(cred);
    const resourceId = await restClient.protection.getResourceIdByResourceName(resourceName);

    getLogger().manager.debug('Check delete permission for recordId and resourceId', {
      recordId,
      resourceId,
    });

    const userClient = CustomKeycloakAdminClient.createFromToken(token);

    return await userClient.openIdConnect.hasPermission({
      audience: cred.clientId,
      permission: `${resourceId}#${Scope.DELETE}`,
    });
  }

  public async validateToken(cred: TCred, token: string): Promise<boolean> {
    const parsedToken = parseToken(token);
    const restClient = new CustomKeycloakAdminClient({
      baseUrl: parsedToken.baseUrl,
      realmName: parsedToken.realmName,
      requestConfig: {
        auth: {
          username: cred.clientId,
          password: cred.clientSecret,
        },
      },
    });

    return await restClient.openIdConnect.validateToken(token);
  }

  public async fetchUserInfo(token: string): Promise<TUserInfo> {
    const userClient = CustomKeycloakAdminClient.createFromToken(token);
    const userInfo = await userClient.openIdConnect.userInfo();

    return {
      id: userInfo.sub,
      email: userInfo.email || '',
      displayName: userInfo.name,
      realmName: userClient.realmName,
    };
  }

  public async fetchToken(cred: TCred, userName: string, password: string): Promise<TToken> {
    const restClient = new CustomKeycloakAdminClient(this.config);
    const resp = await restClient.openIdConnect.token(cred, userName, password);

    return {
      accessToken: resp.access_token,
      refreshToken: resp.refresh_token,
      expiresIn: resp.expires_in,
      refreshExpiresIn: resp.refresh_expires_in,
    };
  }

  public async refreshToken(cred: TCred, refreshToken: string): Promise<TToken> {
    const restClient = new CustomKeycloakAdminClient(this.config);
    const resp = await restClient.openIdConnect.refreshToken(cred, refreshToken);

    return {
      accessToken: resp.access_token,
      refreshToken: resp.refresh_token,
      expiresIn: resp.expires_in,
      refreshExpiresIn: resp.refresh_expires_in,
    };
  }

  public async fetchResourceNamesByUserId(cred: TCred, userId: string): Promise<TResourceDesc[]> {
    const restClient = await this.getRestClient(cred);
    const groups = await restClient.users.listGroups({ id: userId });
    const resourceGroups = groups.filter(isResourceGroups);
    const resourceGroupMap = toResourceGroupMap(resourceGroups);
    const names = resourceGroups.map(getResourceNameByGroup);
    const resourceNamesWithGroups = uniq(names).map((resourceName) => {
      return {
        name: resourceName,
        groups: resourceGroupMap[resourceName].groups,
      };
    });

    getLogger().manager.debug('Resources for userId', {
      userId,
      resourceNamesWithGroups,
    });

    return resourceNamesWithGroups;
  }

  public async fetchUserIdsByResourceId(cred: TCred, recordId: string): Promise<string[]> {
    const restClient = await this.getRestClient(cred);
    const foundGroups = await restClient.groups.find({
      search: getResourceGroupName(recordId),
    });

    getLogger().manager.debug('Found groups for recordId', {
      recordId,
      foundGroups,
    });

    if (foundGroups.length !== 1) {
      return [];
    }

    const targetGroup = foundGroups[0];
    const groupIds = targetGroup.subGroups.map((group) => group.id);
    const promises = groupIds.map((id) => restClient.groups.listMembers({ id }));
    const users = (await Promise.all(promises)).flat();
    const userIds = users.map((u) => u.id);
    const uniqUserIds = uniq(userIds);

    getLogger().manager.debug('Members of groups for recordId', {
      recordId,
      userIds,
    });

    return uniqUserIds;
  }
}
