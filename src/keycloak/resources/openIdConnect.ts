import CustomKeycloakAdminClient from '../CustomKeycloakAdminClient';
import Resource from 'keycloak-admin/lib/resources/resource';
import { StatusCodes } from 'http-status-codes';
import { TCred } from '../../index';

export type TPermissionAccessToken = {
  access_token: string; // eslint-disable-line camelcase
  expires_in: number; // eslint-disable-line camelcase
  refresh_expires_in: number; // eslint-disable-line camelcase
  refresh_token: string; // eslint-disable-line camelcase
};

export type TKeycloakUserInfo = {
  sub: string;
  email_verified: boolean; // eslint-disable-line camelcase
  name: string;
  preferred_username: string; // eslint-disable-line camelcase
  given_name: string; // eslint-disable-line camelcase
  family_name: string; // eslint-disable-line camelcase
  email: string;
};

export class OpenIdConnect extends Resource<{ realm?: string }> {
  public permissionAccessToken = async (cred: TCred): Promise<TPermissionAccessToken> =>
    this.makeRequest<{ payload: URLSearchParams }, TPermissionAccessToken>({
      method: 'POST',
      path: '/token',
      payloadKey: 'payload',
    })({
      payload: new URLSearchParams({
        grant_type: cred.grantType, // eslint-disable-line camelcase
        client_id: cred.clientId, // eslint-disable-line camelcase
        client_secret: cred.clientSecret, // eslint-disable-line camelcase
      }),
    });

  public hasPermission = async ({ audience, permission }: { audience: string; permission: string }): Promise<boolean> => {
    try {
      await this.makeRequest<{ payload: URLSearchParams }, void>({
        method: 'POST',
        path: '/token',
        payloadKey: 'payload',
      })({
        payload: new URLSearchParams({
          audience,
          permission,
          grant_type: 'urn:ietf:params:oauth:grant-type:uma-ticket', // eslint-disable-line camelcase
          response_mode: `permissions`, // eslint-disable-line camelcase
        }),
      });
    } catch (e) {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
      if (e.response?.status === StatusCodes.FORBIDDEN) {
        return false;
      }

      throw e;
    }

    return true;
  };

  public validateToken = async (token: string): Promise<boolean> => {
    const resp = await this.makeRequest<{ payload: URLSearchParams }, { active: boolean }>({
      method: 'POST',
      path: '/token/introspect',
      payloadKey: 'payload',
    })({
      payload: new URLSearchParams({
        token,
      }),
    });

    return resp.active;
  };

  public userInfo = this.makeRequest<void, TKeycloakUserInfo>({
    method: 'GET',
    path: '/userinfo',
  });

  public token = async (cred: TCred, username: string, password: string): Promise<TPermissionAccessToken> =>
    this.makeRequest<{ payload: URLSearchParams }, TPermissionAccessToken>({
      method: 'POST',
      path: '/token',
      payloadKey: 'payload',
    })({
      payload: new URLSearchParams({
        grant_type: cred.grantType, // eslint-disable-line camelcase
        client_id: cred.clientId, // eslint-disable-line camelcase
        client_secret: cred.clientSecret, // eslint-disable-line camelcase
        password: password,
        username: username,
      }),
    });

  public refreshToken = async (cred: TCred, refreshToken: string): Promise<TPermissionAccessToken> =>
    this.makeRequest<{ payload: URLSearchParams }, TPermissionAccessToken>({
      method: 'POST',
      path: '/token',
      payloadKey: 'payload',
    })({
      payload: new URLSearchParams({
        grant_type: cred.grantType, // eslint-disable-line camelcase
        client_id: cred.clientId, // eslint-disable-line camelcase
        client_secret: cred.clientSecret, // eslint-disable-line camelcase
        refresh_token: refreshToken, // eslint-disable-line camelcase
      }),
    });

  constructor(client: CustomKeycloakAdminClient) {
    super(client, {
      path: '/realms/{realm}/protocol/openid-connect',
      getUrlParams: () => ({
        realm: client.realmName,
      }),
      getBaseUrl: () => client.baseUrl,
    });
  }
}
