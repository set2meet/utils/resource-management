import CustomKeycloakAdminClient from '../CustomKeycloakAdminClient';
import Resource from 'keycloak-admin/lib/resources/resource';
import { Scope } from '../../index';

export class Protection extends Resource<{ realm?: string }> {
  public createRecordingResourceDescription = async (resourceName: string): Promise<string> => {
    const resp = await this.makeRequest<
      {
        name: string;
        ownerManagedAccess: boolean;
        resource_scopes: Array<Scope>; // eslint-disable-line camelcase
      },
      { _id: string }
    >({
      method: 'POST',
      path: '/resource_set',
    })({
      name: resourceName,
      ownerManagedAccess: true,
      resource_scopes: [Scope.READ, Scope.DELETE], // eslint-disable-line camelcase
    });

    return resp._id;
  };

  public deleteRecordingResourceDescription = this.makeRequest<{ resourceId: string }, void>({
    method: 'DELETE',
    path: '/resource_set/{resourceId}',
    urlParamKeys: ['resourceId'],
  });

  public createUmaPolicy = this.makeRequest<
    {
      resourceId: string;
      name: string;
      groups: Array<string>;
      scopes: Array<string>;
    },
    { _id: string }
  >({
    method: 'POST',
    path: '/uma-policy/{resourceId}',
    urlParamKeys: ['resourceId'],
  });

  public getUmaPolicies = this.makeRequest<void, Record<string, unknown>>({
    method: 'GET',
    path: '/uma-policy',
  });

  public getResourceIdByResourceName = async (resourceName: string): Promise<string> => {
    const ids = await this.makeRequest<{ resourceName: string }, string[]>({
      method: 'GET',
      path: '/resource_set?name={resourceName}',
      urlParamKeys: ['resourceName'],
    })({ resourceName });

    if (ids.length !== 1) {
      throw new Error(`Cannot find resource by name ${resourceName}`);
    }

    return ids[0];
  };

  constructor(client: CustomKeycloakAdminClient) {
    super(client, {
      path: '/realms/{realm}/authz/protection',
      getUrlParams: () => ({
        realm: client.realmName,
      }),
      getBaseUrl: () => client.baseUrl,
    });
  }
}
