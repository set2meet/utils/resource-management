import { mock } from 'jest-mock-extended';
import CustomKeycloakAdminClient from '../../CustomKeycloakAdminClient';
import { OpenIdConnect } from '../openIdConnect';
import { StatusCodes } from 'http-status-codes';
import { GrantType, TCred } from '../../../index';

/* eslint-disable
@typescript-eslint/no-unsafe-member-access,
@typescript-eslint/no-unsafe-assignment,
@typescript-eslint/no-unsafe-call,
max-statements */

jest.mock('keycloak-admin/lib/resources/resource', () => {
  class Res {
    makeRequest = jest.fn();
  }

  return Res;
});

describe('test OpenIdConnect api', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  const customKeycloakAdminClient = mock<CustomKeycloakAdminClient>();
  const openIdConnect = new OpenIdConnect(customKeycloakAdminClient);

  test('permissionAccessToken', async () => {
    const grantType = GrantType.CLIENT_CREDENTIALS;
    const clientId = 'clientId';
    const clientSecret = 'clientSecret';
    const makeRequest = jest.fn().mockReturnValue(Promise.resolve('permissionAccessToken'));
    const makeRequestFactory = jest.fn().mockReturnValue(makeRequest);

    openIdConnect.makeRequest = makeRequestFactory;

    await openIdConnect.permissionAccessToken({
      grantType,
      clientId,
      clientSecret,
    });

    expect(makeRequestFactory).toBeCalled();
    expect(makeRequestFactory.mock.calls[0][0]).toEqual({
      method: 'POST',
      path: '/token',
      payloadKey: 'payload',
    });
    expect(makeRequest).toBeCalled();

    const payload = makeRequest.mock.calls[0][0].payload;

    expect(payload.toString()).toBe(`grant_type=${grantType}&client_id=${clientId}&client_secret=${clientSecret}`);
  });

  test('hasPermission, token has permission', async () => {
    const audience = 'clientId1';
    const permission = 'resourceId1';

    const makeRequest = jest.fn().mockReturnValue(Promise.resolve());
    const makeRequestFactory = jest.fn().mockReturnValue(makeRequest);

    openIdConnect.makeRequest = makeRequestFactory;

    const result = await openIdConnect.hasPermission({ audience, permission });

    expect(makeRequestFactory).toBeCalled();
    expect(makeRequestFactory.mock.calls[0][0]).toEqual({
      method: 'POST',
      path: '/token',
      payloadKey: 'payload',
    });
    expect(makeRequest).toBeCalled();

    const payload = makeRequest.mock.calls[0][0].payload;

    expect(payload.toString()).toBe(
      // eslint-disable-next-line max-len
      `audience=${audience}&permission=${permission}&grant_type=urn%3Aietf%3Aparams%3Aoauth%3Agrant-type%3Auma-ticket&response_mode=permissions`
    );
    expect(result).toBeTruthy();
  });

  test('hasPermission, token does not have permission', async () => {
    const audience = 'clientId1';
    const permission = 'resourceId1';

    const makeRequest = jest.fn().mockReturnValue(Promise.reject({ response: { status: StatusCodes.FORBIDDEN } }));
    const makeRequestFactory = jest.fn().mockReturnValue(makeRequest);

    openIdConnect.makeRequest = makeRequestFactory;

    const result = await openIdConnect.hasPermission({ audience, permission });

    expect(makeRequestFactory).toBeCalled();
    expect(makeRequestFactory.mock.calls[0][0]).toEqual({
      method: 'POST',
      path: '/token',
      payloadKey: 'payload',
    });
    expect(makeRequest).toBeCalled();

    const payload = makeRequest.mock.calls[0][0].payload;

    expect(payload.toString()).toBe(
      // eslint-disable-next-line max-len
      `audience=${audience}&permission=${permission}&grant_type=urn%3Aietf%3Aparams%3Aoauth%3Agrant-type%3Auma-ticket&response_mode=permissions`
    );
    expect(result).toBeFalsy();
  });

  test('hasPermission, some server error', async () => {
    const audience = 'clientId1';
    const permission = 'resourceId1';
    const error = { response: { status: StatusCodes.INTERNAL_SERVER_ERROR } };
    const makeRequest = jest.fn().mockRejectedValue(error);
    const makeRequestFactory = jest.fn().mockReturnValue(makeRequest);

    openIdConnect.makeRequest = makeRequestFactory;

    try {
      await openIdConnect.hasPermission({ audience, permission });

      expect(true).toBeFalsy();
    } catch (e) {
      expect(e).toBe(error);
    }

    expect(makeRequestFactory).toBeCalled();
    expect(makeRequest).toBeCalled();
  });

  test('validateToken', async () => {
    const token = 'token1';
    const makeRequest = jest.fn().mockReturnValue(Promise.resolve('permissionAccessToken'));
    const makeRequestFactory = jest.fn().mockReturnValue(makeRequest);

    openIdConnect.makeRequest = makeRequestFactory;

    await openIdConnect.validateToken(token);

    expect(makeRequestFactory).toBeCalled();
    expect(makeRequestFactory.mock.calls[0][0]).toEqual({
      method: 'POST',
      path: '/token/introspect',
      payloadKey: 'payload',
    });
    expect(makeRequest).toBeCalled();

    const payload = makeRequest.mock.calls[0][0].payload;

    expect(payload.toString()).toBe(`token=${token}`);
  });

  const cred: TCred = {
    clientSecret: 'clientSecret',
    clientId: 'clientId',
    grantType: GrantType.CLIENT_CREDENTIALS,
  };

  const token = {
    access_token: 'access_token', // eslint-disable-line camelcase
    expires_in: 1, // eslint-disable-line camelcase
    refresh_expires_in: 'refresh_expires_in', // eslint-disable-line camelcase
    refresh_token: 'refresh_token', // eslint-disable-line camelcase
  };

  test('token', async () => {
    const makeRequest = jest.fn().mockResolvedValue(token);
    const makeRequestFactory = jest.fn().mockReturnValue(makeRequest);

    openIdConnect.makeRequest = makeRequestFactory;

    const tokenResp = await openIdConnect.token(cred, 'username', 'password');

    expect(makeRequestFactory).toBeCalled();
    expect(makeRequestFactory.mock.calls[0][0]).toEqual({
      method: 'POST',
      path: '/token',
      payloadKey: 'payload',
    });
    expect(makeRequest).toBeCalled();
    expect(tokenResp).toEqual(token);

    const payload = makeRequest.mock.calls[0][0].payload;

    expect(payload.toString()).toBe(
      `grant_type=${GrantType.CLIENT_CREDENTIALS}&client_id=clientId&client_secret=clientSecret&password=password&username=username`
    );
  });

  test('refreshToken', async () => {
    const makeRequest = jest.fn().mockResolvedValue(token);
    const makeRequestFactory = jest.fn().mockReturnValue(makeRequest);

    openIdConnect.makeRequest = makeRequestFactory;

    const tokenResp = await openIdConnect.refreshToken(cred, 'refreshToken');

    expect(makeRequestFactory).toBeCalled();
    expect(makeRequestFactory.mock.calls[0][0]).toEqual({
      method: 'POST',
      path: '/token',
      payloadKey: 'payload',
    });
    expect(makeRequest).toBeCalled();
    expect(tokenResp).toEqual(token);

    const payload = makeRequest.mock.calls[0][0].payload;

    expect(payload.toString()).toBe(
      `grant_type=${GrantType.CLIENT_CREDENTIALS}&client_id=clientId&client_secret=clientSecret&refresh_token=refreshToken`
    );
  });
});
