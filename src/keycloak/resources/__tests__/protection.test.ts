import { Protection } from '../protection';
import { mock } from 'jest-mock-extended';
import CustomKeycloakAdminClient from '../../CustomKeycloakAdminClient';
import Resource from 'keycloak-admin/lib/resources/resource';

/* eslint-disable
@typescript-eslint/no-unsafe-member-access,
@typescript-eslint/no-unsafe-assignment,
@typescript-eslint/no-unsafe-call,
camelcase,
max-statements */

jest.mock('keycloak-admin/lib/resources/resource', () =>
  jest.fn(() => ({
    makeRequest: jest.fn(),
  }))
);

describe('test Protection api', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  const customKeycloakAdminClient = mock<CustomKeycloakAdminClient>();
  const protection = new Protection(customKeycloakAdminClient);

  test('createRecordingResourceDescription', async () => {
    const resourceId = '1111';
    const makeRequest = jest.fn().mockReturnValue(Promise.resolve({ _id: resourceId }));
    const makeRequestFactory = jest.fn().mockReturnValue(makeRequest);

    protection.makeRequest = makeRequestFactory;

    const resourceName = 'resourceName1';
    const id = await protection.createRecordingResourceDescription(resourceName);

    expect(makeRequestFactory).toBeCalled();
    expect(makeRequestFactory.mock.calls[0][0]).toEqual({
      method: 'POST',
      path: '/resource_set',
    });

    expect(makeRequest).toBeCalled();
    expect(makeRequest.mock.calls[0][0]).toEqual({
      name: resourceName,
      ownerManagedAccess: true,
      resource_scopes: ['read', 'delete'],
    });

    expect(id).toBe(resourceId);
  });

  test('getResourceIdByResourceName, resource exists', async () => {
    const resourceId = '1111';
    const makeRequest = jest.fn().mockReturnValue(Promise.resolve([resourceId]));
    const makeRequestFactory = jest.fn().mockReturnValue(makeRequest);

    protection.makeRequest = makeRequestFactory;

    const resourceName = 'resourceName1';
    const id = await protection.getResourceIdByResourceName(resourceName);

    expect(makeRequestFactory).toBeCalled();
    expect(makeRequestFactory.mock.calls[0][0]).toEqual({
      method: 'GET',
      path: '/resource_set?name={resourceName}',
      urlParamKeys: ['resourceName'],
    });

    expect(makeRequest).toBeCalled();
    expect(makeRequest.mock.calls[0][0]).toEqual({ resourceName });

    expect(id).toBe(resourceId);
  });

  test('getResourceIdByResourceName, resource does not exists', async () => {
    const resourceName = 'resourceName1';
    const makeRequest = jest.fn().mockReturnValue(Promise.resolve([]));

    protection.makeRequest = jest.fn().mockReturnValue(makeRequest);

    await expect(protection.getResourceIdByResourceName(resourceName)).rejects.toThrow('Cannot find resource by name ' + resourceName);
  });

  test('protection constructor', () => {
    const client = mock<CustomKeycloakAdminClient>({
      realmName: 'realm1',
      baseUrl: 'baseUrl1',
    });

    new Protection(client);

    expect((Resource as jest.Mock).mock.calls[0][1].getUrlParams()).toEqual({
      realm: 'realm1',
    });

    expect((Resource as jest.Mock).mock.calls[0][1].getBaseUrl()).toBe('baseUrl1');
  });
});
