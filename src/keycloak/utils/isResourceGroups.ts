import { GroupName } from '../../index';
import GroupRepresentation from 'keycloak-admin/lib/defs/groupRepresentation';

const isResourceGroups = (group: GroupRepresentation): boolean => group.name === GroupName.viewers || group.name === GroupName.owners;

export default isResourceGroups;
