import set from 'lodash/set';
import { TResourceGroupMap } from '../../index';
import getResourceNameByGroup from './getResourceNameByGroup';
import GroupRepresentation from 'keycloak-admin/lib/defs/groupRepresentation';

const toResourceGroupMap = (resourceGroups: GroupRepresentation[]): TResourceGroupMap => {
  const resourceGroupsCache: TResourceGroupMap = {};

  for (const group of resourceGroups) {
    const resourceName = getResourceNameByGroup(group);
    const pos = resourceGroupsCache[resourceName]?.groups?.length || 0;

    set(resourceGroupsCache, `${resourceName}.groups[${pos}]`, group.name);
  }

  return resourceGroupsCache;
};

export default toResourceGroupMap;
