import { RESOURCE_NAME_PREFIX } from './constants';

const getResourceGroupName = (recordId: string): string => `${RESOURCE_NAME_PREFIX}${recordId}`;

export default getResourceGroupName;
