import GroupRepresentation from 'keycloak-admin/lib/defs/groupRepresentation';
import { RESOURCE_NAME_PREFIX } from './constants';

const PREFIX_LENGTH = RESOURCE_NAME_PREFIX.length;

const getResourceNameByGroup = (group: GroupRepresentation): string =>
  group.path.substring(PREFIX_LENGTH + 1, group.path.length - group.name.length - 1);

export default getResourceNameByGroup;
