import { StatusCodes } from 'http-status-codes';
import KeycloakApi from '../KeycloakApi';
import jwt from 'jsonwebtoken';
import { OpenIdConnect } from '../resources/openIdConnect';
import { Protection } from '../resources/protection';
import { Groups } from 'keycloak-admin/lib/resources/groups';
import { Users } from 'keycloak-admin/lib/resources/users';
import { GrantType, GroupName, TConfig, TCred } from '../../index';
import parseToken from '../../utils/parseToken';

jest.mock('../resources/openIdConnect');
jest.mock('../resources/protection');
jest.mock('keycloak-admin/lib/resources/groups');
jest.mock('keycloak-admin/lib/resources/users');
jest.mock('../../utils/parseToken');

const identity = <T>(v: T): T => v;

/* eslint-disable
@typescript-eslint/no-unsafe-call,
@typescript-eslint/no-unsafe-member-access,
max-statements,
camelcase*/

const addToGroup = jest.fn();
const listGroups = jest.fn();

(Users as jest.Mock).mockImplementation(() => ({
  addToGroup,
  listGroups,
}));

const createGroup = jest.fn();
const findGroup = jest.fn();
const findOneGroup = jest.fn();
const setOrCreateChild = jest.fn();
const delGroup = jest.fn();
const listMembers = jest.fn();

(Groups as jest.Mock).mockImplementation(() => ({
  create: createGroup,
  find: findGroup,
  setOrCreateChild: setOrCreateChild,
  findOne: findOneGroup,
  del: delGroup,
  listMembers: listMembers,
}));

const validateToken = jest.fn();
const permissionAccessToken = jest.fn();
const hasPermission = jest.fn();
const userInfo = jest.fn();
const token = jest.fn();
const refreshToken = jest.fn();

(OpenIdConnect as jest.Mock).mockImplementation(() => ({
  validateToken,
  permissionAccessToken,
  hasPermission,
  userInfo,
  token,
  refreshToken,
}));

const getResourceIdByResourceName = jest.fn();
const createRecordingResourceDescription = jest.fn();
const createUmaPolicy = jest.fn();
const deleteRecordingResourceDescription = jest.fn();

(Protection as jest.Mock).mockImplementation(() => ({
  getResourceIdByResourceName,
  createRecordingResourceDescription,
  createUmaPolicy,
  deleteRecordingResourceDescription,
}));

jest.mock('jsonwebtoken', () => {
  return {
    decode: jest.fn(),
  };
});

describe('test KeycloakApi', () => {
  const conf: TConfig = {
    baseUrl: 'baseUrl',
    realmName: 'realmName',
  };

  const cred: TCred = {
    grantType: GrantType.CLIENT_CREDENTIALS,
    clientId: 'clientId',
    clientSecret: 'clientSecret',
  };

  const keycloakApi = new KeycloakApi(conf);

  beforeEach(() => {
    jest.clearAllMocks();
  });

  test('test hasReadPermissions', async () => {
    const resourceName = 'resourceName1';
    const resourceId = 'id_of_resourceName1';

    (parseToken as jest.Mock).mockImplementation(identity);

    permissionAccessToken.mockReturnValue({
      access_token: 'pat_access_token', // eslint-disable-line camelcase
    });

    getResourceIdByResourceName.mockReturnValue(resourceId);
    hasPermission.mockReturnValue(true);

    const result1 = await keycloakApi.hasReadPermissions(cred, resourceName, 'token1');

    expect(permissionAccessToken).toBeCalled();
    expect(permissionAccessToken.mock.calls[0][0]).toEqual(cred);
    expect(getResourceIdByResourceName.mock.calls[0][0]).toBe('record_resourceName1');
    expect(hasPermission.mock.calls[0][0]).toEqual({
      audience: cred.clientId,
      permission: 'id_of_resourceName1#read',
    });
    expect(result1).toBeTruthy();
  });

  test('test hasDeletePermissions', async () => {
    const resourceName = 'resourceName1';
    const resourceId = 'id_of_resourceName1';

    (parseToken as jest.Mock).mockImplementation(identity);

    permissionAccessToken.mockReturnValue({
      access_token: 'pat_access_token', // eslint-disable-line camelcase
    });

    getResourceIdByResourceName.mockReturnValue(resourceId);
    hasPermission.mockReturnValue(true);

    const result1 = await keycloakApi.hasDeletePermissions(cred, resourceName, 'token1');

    expect(getResourceIdByResourceName.mock.calls[0][0]).toBe('record_resourceName1');
    expect(hasPermission.mock.calls[0][0]).toEqual({
      audience: cred.clientId,
      permission: 'id_of_resourceName1#delete',
    });
    expect(result1).toBeTruthy();
  });

  test('test validateToken invokes openIdConnect.validateToken', async () => {
    validateToken.mockReturnValue(false);

    (parseToken as jest.Mock).mockImplementation(identity);
    (jwt.decode as jest.Mock).mockReturnValue({
      payload: {
        iss: 'https://dev.set2meet.com/auth/realms/Set2Meet',
      },
    });

    const result1 = await keycloakApi.validateToken(cred, 'false_token');

    expect(result1).toBeFalsy();

    validateToken.mockReturnValue(true);

    const result2 = await keycloakApi.validateToken(cred, 'true_token');

    expect(result2).toBeTruthy();
  });

  test('test assocResourceWithUsers1', async () => {
    const resourceName = 'resourceName2';
    const userIds = ['user1', 'user2'];
    const resourceId = 'id_of_resourceName2';
    const groupId = 'groupId';

    permissionAccessToken.mockReturnValue({
      access_token: 'pat_access_token', // eslint-disable-line camelcase
    });

    createRecordingResourceDescription.mockReturnValue(Promise.resolve(resourceId));
    getResourceIdByResourceName.mockReturnValue(resourceId);
    createGroup.mockReturnValue(Promise.resolve());
    findGroup.mockReturnValue(
      Promise.resolve([
        {
          id: groupId,
          name: 'record_' + resourceName,
          subGroups: [{ id: 'subgroup1', name: 'preview' }],
        },
      ])
    );
    createUmaPolicy.mockReturnValue(Promise.resolve());
    setOrCreateChild.mockResolvedValue({ id: 'subgoup1' });
    findOneGroup.mockResolvedValue({ id: 'subgoup1', path: 'path1' });

    await keycloakApi.assocResourceWithUsers(cred, resourceName, {
      viewers: userIds,
      owners: [userIds[0]],
    });

    expect(createRecordingResourceDescription.mock.calls[0][0]).toBe('record_' + resourceName);
    expect(createGroup.mock.calls[0][0]).toEqual({ name: 'record_' + resourceName });
    expect(findGroup.mock.calls[0][0]).toEqual({ search: 'record_' + resourceName });

    expect(createUmaPolicy.mock.calls[0][0]).toEqual({
      name: `Allow read path1`,
      resourceId: 'id_of_resourceName2',
      groups: ['path1'],
      scopes: ['read'],
    });
    expect(createUmaPolicy.mock.calls[1][0]).toEqual({
      name: `Allow delete,read path1`,
      resourceId: 'id_of_resourceName2',
      groups: ['path1'],
      scopes: ['delete', 'read'],
    });

    expect(addToGroup.mock.calls.length).toBe(userIds.length + 1);
    expect(addToGroup.mock.calls[0][0]).toEqual({
      id: userIds[0],
      groupId: 'subgoup1',
    });
    expect(addToGroup.mock.calls[1][0]).toEqual({
      id: userIds[1],
      groupId: 'subgoup1',
    });
    //eslint-disable-next-line no-magic-numbers
    expect(addToGroup.mock.calls[2][0]).toEqual({
      id: userIds[0],
      groupId: 'subgoup1',
    });
  });

  test('test assocResourceWithUsers1, wrong resource name', async () => {
    const resourceName = 'resourceName2/';
    const userIds = ['user1', 'user2'];
    const resourceId = 'resourceId1';

    createRecordingResourceDescription.mockReturnValue(Promise.resolve(resourceId));
    permissionAccessToken.mockReturnValue({
      access_token: 'pat_access_token', // eslint-disable-line camelcase
    });

    await expect(() =>
      keycloakApi.assocResourceWithUsers(cred, resourceName, {
        viewers: userIds,
        owners: [userIds[0]],
      })
    ).rejects.toThrowError('Incorrect group name.');
  });

  test('test assocResourceWithUsers retry2', async () => {
    const resourceName = 'resourceName2';
    const userIds = ['user3'];
    const groupId = 'groupId';
    const conflictError = { response: { status: StatusCodes.CONFLICT } };
    const resourceId = '1123';

    permissionAccessToken.mockReturnValue({
      access_token: 'pat_access_token', // eslint-disable-line camelcase
    });

    createRecordingResourceDescription.mockRejectedValue(conflictError);
    getResourceIdByResourceName.mockReturnValue(resourceId);
    createGroup.mockReturnValue(Promise.reject(conflictError));
    findGroup.mockReturnValue(
      Promise.resolve([
        {
          id: groupId,
          name: 'record_resourceName2',
          subGroups: [{ id: 'subgroup1', name: 'preview' }],
        },
      ])
    );

    createUmaPolicy.mockReturnValue(Promise.reject(conflictError));

    await keycloakApi.assocResourceWithUsers(cred, resourceName, {
      viewers: userIds,
    });
    expect(addToGroup.mock.calls[0][0]).toEqual({
      id: userIds[0],
      groupId: 'subgoup1',
    });
  });

  test('test assocResourceWithUsers errors', async () => {
    const resourceName = 'resourceName2';
    const userIds = ['user3'];
    const resourceId = 'id_of_resourceName2';

    permissionAccessToken.mockReturnValue({
      access_token: 'pat_access_token', // eslint-disable-line camelcase
    });

    createRecordingResourceDescription.mockReturnValue(Promise.reject(Error('unknown error')));
    createGroup.mockReturnValue(Promise.resolve());
    createUmaPolicy.mockReturnValue(Promise.resolve());

    try {
      await keycloakApi.assocResourceWithUsers(cred, resourceName, {
        viewers: [userIds[1]],
      });

      expect(true).toBeFalsy();
    } catch (e) {
      expect(e.message).toEqual('unknown error');
    }

    createRecordingResourceDescription.mockReturnValue(Promise.resolve(resourceId));
    getResourceIdByResourceName.mockReturnValue(resourceId);
    createGroup.mockReturnValue(Promise.resolve());
    findGroup.mockReturnValue(Promise.resolve([]));
    createUmaPolicy.mockReturnValue(Promise.resolve());

    try {
      await keycloakApi.assocResourceWithUsers(cred, resourceName, {
        viewers: [userIds[1]],
      });

      expect(true).toBeFalsy();
    } catch (e) {
      expect(e.message.startsWith('Cannot create or find group with name ')).toBeTruthy();
    }
  });

  test('test fetchUserInfo', async () => {
    (parseToken as jest.Mock).mockReturnValue({
      baseUrl: 'baseUrl',
      realmName: 'realmName',
    });

    userInfo.mockResolvedValue({
      name: 'displayName',
      email: 'email',
      sub: '123',
      realmName: 'realmName',
    });

    const result1 = await keycloakApi.fetchUserInfo('token1');

    expect(userInfo).toBeCalled();
    expect(result1).toEqual({
      displayName: 'displayName',
      email: 'email',
      id: '123',
      realmName: 'realmName',
    });
  });

  test('test fetchToken', async () => {
    token.mockResolvedValue({
      access_token: 'access_token',
      refresh_token: 'refresh_token',
      expires_in: 1,
      refresh_expires_in: 1,
    });

    const result = await keycloakApi.fetchToken(cred, 'username', 'password');

    expect(token).toBeCalled();
    expect(result).toEqual({
      accessToken: 'access_token',
      refreshToken: 'refresh_token',
      expiresIn: 1,
      refreshExpiresIn: 1,
    });
  });

  test('test refreshToken', async () => {
    refreshToken.mockResolvedValue({
      access_token: 'access_token',
      refresh_token: 'refresh_token',
      expires_in: 1,
      refresh_expires_in: 1,
    });

    const result = await keycloakApi.refreshToken(cred, 'refreshToken');

    expect(refreshToken).toBeCalled();
    expect(result).toEqual({
      accessToken: 'access_token',
      refreshToken: 'refresh_token',
      expiresIn: 1,
      refreshExpiresIn: 1,
    });
  });

  test('test dissocResourceWithAllUsers', async () => {
    const resourceName = 'resourceName2';
    const resourceId = 'resourceId1';

    permissionAccessToken.mockReturnValue({
      access_token: 'pat_access_token', // eslint-disable-line camelcase
    });

    getResourceIdByResourceName.mockResolvedValue(resourceId);

    findGroup.mockReturnValue(
      Promise.resolve([
        {
          id: 'groupId',
          name: resourceName,
          subGroups: [{ id: 'subgroup1', name: 'preview' }],
        },
      ])
    );

    await keycloakApi.dissocResourceWithAllUsers(cred, resourceName);

    expect(deleteRecordingResourceDescription).toBeCalledWith({ resourceId });
    expect(delGroup).toBeCalledWith({ id: 'groupId' });
  });

  test('test dissocResourceWithAllUsers, inconsistent resource state', async () => {
    const resourceName = 'resourceName2';
    const resourceId = 'resourceId1';

    permissionAccessToken.mockReturnValue({
      access_token: 'pat_access_token', // eslint-disable-line camelcase
    });

    getResourceIdByResourceName.mockResolvedValue(resourceId);

    findGroup.mockReturnValue(
      Promise.resolve([
        {
          id: 'groupId',
          name: resourceName,
          subGroups: [{ id: 'subgroup1', name: 'preview' }],
        },
        {
          id: 'groupId2',
          name: resourceName + '_2',
          subGroups: [{ id: 'subgroup1', name: 'preview' }],
        },
      ])
    );

    await expect(() => keycloakApi.dissocResourceWithAllUsers(cred, resourceName)).rejects.toThrowError(
      /State of policy-groups inconsistent.*/
    );
  });

  test('test fetchResourceNamesByUserId', async () => {
    (parseToken as jest.Mock).mockImplementation(identity);

    permissionAccessToken.mockReturnValue({
      access_token: 'pat_access_token', // eslint-disable-line camelcase
    });

    listGroups.mockResolvedValue([
      {
        id: 'groupId_1',
        name: 'viewers',
        path: '/record_resourceName/viewers',
      },
      {
        id: 'groupId_2',
        name: 'owners',
        path: '/record_resourceName/owners',
      },
      {
        id: 'groupId_3',
        name: 'some group',
        path: '/record_resourceName/some',
      },
    ]);

    const names = await keycloakApi.fetchResourceNamesByUserId(cred, 'user1');

    expect(names).toEqual([
      {
        name: 'resourceName',
        groups: [GroupName.viewers, GroupName.owners],
      },
    ]);
  });

  test('test fetchUserIdsByResourceId', async () => {
    (parseToken as jest.Mock).mockImplementation(identity);

    permissionAccessToken.mockReturnValue({
      access_token: 'pat_access_token', // eslint-disable-line camelcase
    });

    findGroup.mockResolvedValue([
      {
        id: 'groupId_1',
        name: 'resourceName',
        path: '/record_resourceName',
        subGroups: [
          {
            id: 'groupId_1_1',
            name: 'owners',
            path: '/record_resourceName/owners',
          },
          {
            id: 'groupId_1_2',
            name: 'viewers',
            path: '/record_resourceName/viewers',
          },
        ],
      },
    ]);

    listMembers.mockResolvedValueOnce([{ id: 'user1' }, { id: 'user2' }]).mockResolvedValueOnce({ id: 'user2' });

    const names = await keycloakApi.fetchUserIdsByResourceId(cred, 'resourceName');

    expect(findGroup).toBeCalledWith({
      search: 'record_resourceName',
    });
    expect(names).toEqual(['user1', 'user2']);
  });

  test('test fetchUserIdsByResourceId, group is not exists', async () => {
    (parseToken as jest.Mock).mockImplementation(identity);

    permissionAccessToken.mockReturnValue({
      access_token: 'pat_access_token', // eslint-disable-line camelcase
    });

    findGroup.mockResolvedValue([]);

    const names = await keycloakApi.fetchUserIdsByResourceId(cred, 'resourceName');

    expect(names).toEqual([]);
  });
});
