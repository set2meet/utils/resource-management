import { ConnectionConfig, KeycloakAdminClient } from 'keycloak-admin/lib/client';
import { OpenIdConnect } from './resources/openIdConnect';
import { Protection } from './resources/protection';
import { TParsedToken } from '../utils/parseToken';
import parseToken from '../utils/parseToken';

export default class CustomKeycloakAdminClient extends KeycloakAdminClient {
  public openIdConnect: OpenIdConnect;
  public protection: Protection;

  constructor(connectionConfig?: ConnectionConfig) {
    super(connectionConfig);

    this.openIdConnect = new OpenIdConnect(this);
    this.protection = new Protection(this);
  }

  public static createFromParsedToken(parsedToken: TParsedToken): CustomKeycloakAdminClient {
    return new CustomKeycloakAdminClient({
      baseUrl: parsedToken.baseUrl,
      realmName: parsedToken.realmName,
    });
  }

  public static createFromToken(token: string): CustomKeycloakAdminClient {
    const parsedToken = parseToken(token);
    const userClient = CustomKeycloakAdminClient.createFromParsedToken(parsedToken);

    userClient.setAccessToken(token);

    return userClient;
  }
}
