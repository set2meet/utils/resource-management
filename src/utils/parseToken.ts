import jwt from 'jsonwebtoken';

export type TParsedToken = {
  baseUrl: string;
  realmName: string;
  payload: {
    iss: string;
    azp: string;
    exp: number;
    iat: number;
    sub: string;
    name: string;
  };
};

const parseToken = (token: string): TParsedToken => {
  try {
    const decoded = jwt.decode(token, {
      complete: true,
      json: true,
    }) as Partial<TParsedToken>;
    const absoluteRealmUrl = decoded.payload.iss;
    const [baseUrl, realmName] = absoluteRealmUrl.split('/realms/');

    return {
      baseUrl,
      realmName,
      ...decoded,
    } as TParsedToken;
  } catch (e) {
    throw new Error(`Cannot parse token: ${token}`);
  }
};

export default parseToken;
