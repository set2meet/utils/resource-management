import bearerToken from '../bearerToken';

test('bearerToken', () => {
  const header1 = 'bearer 131313';
  const bearer1 = bearerToken(header1);

  expect(bearer1).toBe('131313');

  const header2 = '131313';
  const bearer2 = bearerToken(header2);

  expect(bearer2).toBe('131313');
});
