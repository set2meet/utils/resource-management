import bearerToken from './utils/bearerToken';
import parseToken from './utils/parseToken';
import { registerLogger } from './logger/logger';
export declare type TConfig = {
  baseUrl?: string;
  realmName?: string;
};
export declare enum GrantType {
  REFRESH_TOKEN = 'refresh_token',
  PASSWORD = 'password',
  CLIENT_CREDENTIALS = 'client_credentials',
}
export declare type TCred = {
  grantType: GrantType;
  clientId: string;
  clientSecret: string;
};
export declare type TUserInfo = {
  displayName: string;
  email: string;
  id: string;
  realmName: string;
};
export declare type TToken = {
  accessToken: string;
  refreshToken: string;
  expiresIn: number;
  refreshExpiresIn: number;
};
export declare type TGroupedUsers = {
  viewers?: Array<string>;
  owners?: Array<string>;
};
export declare enum GroupName {
  viewers = 'viewers',
  owners = 'owners',
}
export declare type TResourceDesc = {
  name: string;
  groups: GroupName[];
};
export declare type TResourceGroupMap = {
  [key: string]: {
    groups: GroupName[];
  };
};
export declare enum Scope {
  READ = 'read',
  DELETE = 'delete',
}
export { bearerToken, parseToken, registerLogger };
export interface IResourceApi {
  assocResourceWithUsers(cred: TCred, recordId: string, groupedUsers: TGroupedUsers): Promise<void>;
  dissocResourceWithAllUsers(cred: TCred, recordId: string): Promise<void>;
  hasReadPermissions(cred: TCred, recordId: string, token: string): Promise<boolean>;
  hasDeletePermissions(cred: TCred, recordId: string, token: string): Promise<boolean>;
  validateToken(cred: TCred, token: string): Promise<boolean>;
  fetchUserInfo(token: string): Promise<TUserInfo>;
  fetchToken(cred: TCred, userName: string, password: string): Promise<TToken>;
  refreshToken(cred: TCred, refreshToken: string): Promise<TToken>;
  fetchResourceNamesByUserId(cred: TCred, userId: string): Promise<TResourceDesc[]>;
  fetchUserIdsByResourceId(cred: TCred, recordId: string): Promise<string[]>;
}
declare const _default: (config?: TConfig) => IResourceApi;
export default _default;
