"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const client_1 = require("keycloak-admin/lib/client");
const openIdConnect_1 = require("./resources/openIdConnect");
const protection_1 = require("./resources/protection");
const parseToken_1 = __importDefault(require("../utils/parseToken"));
class CustomKeycloakAdminClient extends client_1.KeycloakAdminClient {
    constructor(connectionConfig) {
        super(connectionConfig);
        this.openIdConnect = new openIdConnect_1.OpenIdConnect(this);
        this.protection = new protection_1.Protection(this);
    }
    static createFromParsedToken(parsedToken) {
        return new CustomKeycloakAdminClient({
            baseUrl: parsedToken.baseUrl,
            realmName: parsedToken.realmName,
        });
    }
    static createFromToken(token) {
        const parsedToken = parseToken_1.default(token);
        const userClient = CustomKeycloakAdminClient.createFromParsedToken(parsedToken);
        userClient.setAccessToken(token);
        return userClient;
    }
}
exports.default = CustomKeycloakAdminClient;
