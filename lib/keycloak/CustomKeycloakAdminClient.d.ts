import { ConnectionConfig, KeycloakAdminClient } from 'keycloak-admin/lib/client';
import { OpenIdConnect } from './resources/openIdConnect';
import { Protection } from './resources/protection';
import { TParsedToken } from '../utils/parseToken';
export default class CustomKeycloakAdminClient extends KeycloakAdminClient {
    openIdConnect: OpenIdConnect;
    protection: Protection;
    constructor(connectionConfig?: ConnectionConfig);
    static createFromParsedToken(parsedToken: TParsedToken): CustomKeycloakAdminClient;
    static createFromToken(token: string): CustomKeycloakAdminClient;
}
