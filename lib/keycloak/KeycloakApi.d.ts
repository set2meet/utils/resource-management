import { TCred, IResourceApi, TToken, TUserInfo, TConfig, TGroupedUsers, TResourceDesc } from '../index';
export default class KeycloakApi implements IResourceApi {
  private config?;
  constructor(config?: TConfig);
  private getRestClient;
  assocResourceWithUsers(cred: TCred, recordId: string, groupedUsers: TGroupedUsers): Promise<void>;
  dissocResourceWithAllUsers(cred: TCred, recordId: string): Promise<void>;
  hasReadPermissions(cred: TCred, recordId: string, token: string): Promise<boolean>;
  hasDeletePermissions(cred: TCred, recordId: string, token: string): Promise<boolean>;
  validateToken(cred: TCred, token: string): Promise<boolean>;
  fetchUserInfo(token: string): Promise<TUserInfo>;
  fetchToken(cred: TCred, userName: string, password: string): Promise<TToken>;
  refreshToken(cred: TCred, refreshToken: string): Promise<TToken>;
  fetchResourceNamesByUserId(cred: TCred, userId: string): Promise<TResourceDesc[]>;
  fetchUserIdsByResourceId(cred: TCred, recordId: string): Promise<string[]>;
}
