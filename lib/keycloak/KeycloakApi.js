"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const index_1 = require("../index");
const CustomKeycloakAdminClient_1 = __importDefault(require("./CustomKeycloakAdminClient"));
const http_status_codes_1 = require("http-status-codes");
const parseToken_1 = __importDefault(require("../utils/parseToken"));
const uniq_1 = __importDefault(require("lodash/uniq"));
const getResourceGroupName_1 = __importDefault(require("./utils/getResourceGroupName"));
const getResourceNameByGroup_1 = __importDefault(require("./utils/getResourceNameByGroup"));
const isResourceGroups_1 = __importDefault(require("./utils/isResourceGroups"));
const toResourceGroupMap_1 = __importDefault(require("./utils/toResourceGroupMap"));
const logger_1 = require("../logger/logger");
const continueIfStatusCodeIsConflict = (error) => {
    var _a;
    if (((_a = error === null || error === void 0 ? void 0 : error.response) === null || _a === void 0 ? void 0 : _a.status) !== http_status_codes_1.StatusCodes.CONFLICT) {
        throw error;
    }
};
const createRecordingResourceDescriptionIfNeed = (restClient, resourceName) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        return yield restClient.protection.createRecordingResourceDescription(resourceName);
    }
    catch (e) {
        continueIfStatusCodeIsConflict(e);
    }
    return restClient.protection.getResourceIdByResourceName(resourceName);
});
const createGroupIfNeed = (restClient, { groupName }) => __awaiter(void 0, void 0, void 0, function* () {
    if (groupName.includes('/')) {
        throw new Error('Incorrect group name.');
    }
    try {
        yield restClient.groups.create({
            name: groupName,
        });
        logger_1.getLogger().manager.debug('Created group with groupName', { groupName });
    }
    catch (e) {
        continueIfStatusCodeIsConflict(e);
    }
    const foundGroups = yield restClient.groups.find({
        search: groupName,
    });
    const groups = foundGroups.filter((g) => g.name === groupName);
    if (groups.length === 0) {
        throw new Error(`Cannot create or find group with name ${groupName}`);
    }
    return groups[0];
});
const createResourceGroupPolicyIfNeed = (restClient, { resourceId, groupName, scopes }) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const policyName = `Allow ${scopes.join(',')} ${groupName}`;
        yield restClient.protection.createUmaPolicy({
            resourceId,
            name: policyName,
            groups: [groupName],
            scopes,
        });
        logger_1.getLogger().manager.debug('Created policy for resource', { policyName, resourceId });
    }
    catch (e) {
        continueIfStatusCodeIsConflict(e);
    }
});
const createSubGroupWithPolicy = (restClient, { userIds = [], resourceId, groupName, parentGroup, scopes, }) => __awaiter(void 0, void 0, void 0, function* () {
    let subGroup = parentGroup.subGroups.find((subGroup) => subGroup.name === groupName);
    if (!subGroup) {
        const subGroupId = (yield restClient.groups.setOrCreateChild({ id: parentGroup.id }, { name: groupName })).id;
        logger_1.getLogger().manager.debug('Added subgroup to parent', {
            groupName,
            parentGroupId: parentGroup.id,
        });
        subGroup = yield restClient.groups.findOne({ id: subGroupId });
    }
    yield createResourceGroupPolicyIfNeed(restClient, {
        resourceId,
        groupName: subGroup.path,
        scopes,
    });
    for (const id of userIds) {
        yield restClient.users.addToGroup({ id, groupId: subGroup.id });
    }
});
class KeycloakApi {
    constructor(config) {
        this.config = config;
    }
    getRestClient(cred) {
        return __awaiter(this, void 0, void 0, function* () {
            const patKcAdminClient = new CustomKeycloakAdminClient_1.default(this.config);
            const pat = yield patKcAdminClient.openIdConnect.permissionAccessToken(cred);
            patKcAdminClient.setAccessToken(pat.access_token);
            return patKcAdminClient;
        });
    }
    assocResourceWithUsers(cred, recordId, groupedUsers) {
        return __awaiter(this, void 0, void 0, function* () {
            const resourceName = getResourceGroupName_1.default(recordId);
            const restClient = yield this.getRestClient(cred);
            const resourceId = yield createRecordingResourceDescriptionIfNeed(restClient, resourceName);
            const parentGroup = yield createGroupIfNeed(restClient, {
                groupName: resourceName,
            });
            yield createSubGroupWithPolicy(restClient, {
                userIds: groupedUsers.viewers,
                resourceId,
                groupName: index_1.GroupName.viewers,
                parentGroup,
                scopes: [index_1.Scope.READ],
            });
            yield createSubGroupWithPolicy(restClient, {
                userIds: groupedUsers.owners,
                resourceId,
                groupName: index_1.GroupName.owners,
                parentGroup,
                scopes: [index_1.Scope.DELETE, index_1.Scope.READ],
            });
        });
    }
    dissocResourceWithAllUsers(cred, recordId) {
        return __awaiter(this, void 0, void 0, function* () {
            const resourceName = getResourceGroupName_1.default(recordId);
            const restClient = yield this.getRestClient(cred);
            const resourceId = yield restClient.protection.getResourceIdByResourceName(resourceName);
            const foundGroups = yield restClient.groups.find({
                search: resourceName,
            });
            if (foundGroups.length !== 1) {
                throw new Error(`State of policy-groups inconsistent for resource ${resourceName}.`);
            }
            logger_1.getLogger().manager.debug('Delete resource description by recordId and resourceId', {
                recordId,
                resourceId,
            });
            yield restClient.protection.deleteRecordingResourceDescription({
                resourceId,
            });
            const groupId = foundGroups[0].id;
            logger_1.getLogger().manager.debug('Delete group by recordId, resourceId and groupId', {
                recordId,
                resourceId,
                groupId,
            });
            yield restClient.groups.del({
                id: groupId,
            });
        });
    }
    hasReadPermissions(cred, recordId, token) {
        return __awaiter(this, void 0, void 0, function* () {
            const resourceName = getResourceGroupName_1.default(recordId);
            const restClient = yield this.getRestClient(cred);
            const resourceId = yield restClient.protection.getResourceIdByResourceName(resourceName);
            const userClient = CustomKeycloakAdminClient_1.default.createFromToken(token);
            logger_1.getLogger().manager.debug('Check read permission for recordId and resourceId', {
                recordId,
                resourceId,
            });
            return yield userClient.openIdConnect.hasPermission({
                audience: cred.clientId,
                permission: `${resourceId}#${index_1.Scope.READ}`,
            });
        });
    }
    hasDeletePermissions(cred, recordId, token) {
        return __awaiter(this, void 0, void 0, function* () {
            const resourceName = getResourceGroupName_1.default(recordId);
            const restClient = yield this.getRestClient(cred);
            const resourceId = yield restClient.protection.getResourceIdByResourceName(resourceName);
            logger_1.getLogger().manager.debug('Check delete permission for recordId and resourceId', {
                recordId,
                resourceId,
            });
            const userClient = CustomKeycloakAdminClient_1.default.createFromToken(token);
            return yield userClient.openIdConnect.hasPermission({
                audience: cred.clientId,
                permission: `${resourceId}#${index_1.Scope.DELETE}`,
            });
        });
    }
    validateToken(cred, token) {
        return __awaiter(this, void 0, void 0, function* () {
            const parsedToken = parseToken_1.default(token);
            const restClient = new CustomKeycloakAdminClient_1.default({
                baseUrl: parsedToken.baseUrl,
                realmName: parsedToken.realmName,
                requestConfig: {
                    auth: {
                        username: cred.clientId,
                        password: cred.clientSecret,
                    },
                },
            });
            return yield restClient.openIdConnect.validateToken(token);
        });
    }
    fetchUserInfo(token) {
        return __awaiter(this, void 0, void 0, function* () {
            const userClient = CustomKeycloakAdminClient_1.default.createFromToken(token);
            const userInfo = yield userClient.openIdConnect.userInfo();
            return {
                id: userInfo.sub,
                email: userInfo.email || '',
                displayName: userInfo.name,
                realmName: userClient.realmName,
            };
        });
    }
    fetchToken(cred, userName, password) {
        return __awaiter(this, void 0, void 0, function* () {
            const restClient = new CustomKeycloakAdminClient_1.default(this.config);
            const resp = yield restClient.openIdConnect.token(cred, userName, password);
            return {
                accessToken: resp.access_token,
                refreshToken: resp.refresh_token,
                expiresIn: resp.expires_in,
                refreshExpiresIn: resp.refresh_expires_in,
            };
        });
    }
    refreshToken(cred, refreshToken) {
        return __awaiter(this, void 0, void 0, function* () {
            const restClient = new CustomKeycloakAdminClient_1.default(this.config);
            const resp = yield restClient.openIdConnect.refreshToken(cred, refreshToken);
            return {
                accessToken: resp.access_token,
                refreshToken: resp.refresh_token,
                expiresIn: resp.expires_in,
                refreshExpiresIn: resp.refresh_expires_in,
            };
        });
    }
    fetchResourceNamesByUserId(cred, userId) {
        return __awaiter(this, void 0, void 0, function* () {
            const restClient = yield this.getRestClient(cred);
            const groups = yield restClient.users.listGroups({ id: userId });
            const resourceGroups = groups.filter(isResourceGroups_1.default);
            const resourceGroupMap = toResourceGroupMap_1.default(resourceGroups);
            const names = resourceGroups.map(getResourceNameByGroup_1.default);
            const resourceNamesWithGroups = uniq_1.default(names).map((resourceName) => {
                return {
                    name: resourceName,
                    groups: resourceGroupMap[resourceName].groups,
                };
            });
            logger_1.getLogger().manager.debug('Resources for userId', {
                userId,
                resourceNamesWithGroups,
            });
            return resourceNamesWithGroups;
        });
    }
    fetchUserIdsByResourceId(cred, recordId) {
        return __awaiter(this, void 0, void 0, function* () {
            const restClient = yield this.getRestClient(cred);
            const foundGroups = yield restClient.groups.find({
                search: getResourceGroupName_1.default(recordId),
            });
            logger_1.getLogger().manager.debug('Found groups for recordId', {
                recordId,
                foundGroups,
            });
            if (foundGroups.length !== 1) {
                return [];
            }
            const targetGroup = foundGroups[0];
            const groupIds = targetGroup.subGroups.map((group) => group.id);
            const promises = groupIds.map((id) => restClient.groups.listMembers({ id }));
            const users = (yield Promise.all(promises)).flat();
            const userIds = users.map((u) => u.id);
            const uniqUserIds = uniq_1.default(userIds);
            logger_1.getLogger().manager.debug('Members of groups for recordId', {
                recordId,
                userIds,
            });
            return uniqUserIds;
        });
    }
}
exports.default = KeycloakApi;
