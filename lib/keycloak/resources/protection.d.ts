import CustomKeycloakAdminClient from '../CustomKeycloakAdminClient';
import Resource from 'keycloak-admin/lib/resources/resource';
export declare class Protection extends Resource<{
    realm?: string;
}> {
    createRecordingResourceDescription: (resourceName: string) => Promise<string>;
    deleteRecordingResourceDescription: (payload?: {
        resourceId: string;
    } & {
        realm?: string;
    }) => Promise<void>;
    createUmaPolicy: (payload?: {
        resourceId: string;
        name: string;
        groups: Array<string>;
        scopes: Array<string>;
    } & {
        realm?: string;
    }) => Promise<{
        _id: string;
    }>;
    getUmaPolicies: (payload?: void & {
        realm?: string;
    }) => Promise<Record<string, unknown>>;
    getResourceIdByResourceName: (resourceName: string) => Promise<string>;
    constructor(client: CustomKeycloakAdminClient);
}
