"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OpenIdConnect = void 0;
const resource_1 = __importDefault(require("keycloak-admin/lib/resources/resource"));
const http_status_codes_1 = require("http-status-codes");
class OpenIdConnect extends resource_1.default {
    constructor(client) {
        super(client, {
            path: '/realms/{realm}/protocol/openid-connect',
            getUrlParams: () => ({
                realm: client.realmName,
            }),
            getBaseUrl: () => client.baseUrl,
        });
        this.permissionAccessToken = (cred) => __awaiter(this, void 0, void 0, function* () {
            return this.makeRequest({
                method: 'POST',
                path: '/token',
                payloadKey: 'payload',
            })({
                payload: new URLSearchParams({
                    grant_type: cred.grantType,
                    client_id: cred.clientId,
                    client_secret: cred.clientSecret,
                }),
            });
        });
        this.hasPermission = ({ audience, permission }) => __awaiter(this, void 0, void 0, function* () {
            var _a;
            try {
                yield this.makeRequest({
                    method: 'POST',
                    path: '/token',
                    payloadKey: 'payload',
                })({
                    payload: new URLSearchParams({
                        audience,
                        permission,
                        grant_type: 'urn:ietf:params:oauth:grant-type:uma-ticket',
                        response_mode: `permissions`,
                    }),
                });
            }
            catch (e) {
                // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
                if (((_a = e.response) === null || _a === void 0 ? void 0 : _a.status) === http_status_codes_1.StatusCodes.FORBIDDEN) {
                    return false;
                }
                throw e;
            }
            return true;
        });
        this.validateToken = (token) => __awaiter(this, void 0, void 0, function* () {
            const resp = yield this.makeRequest({
                method: 'POST',
                path: '/token/introspect',
                payloadKey: 'payload',
            })({
                payload: new URLSearchParams({
                    token,
                }),
            });
            return resp.active;
        });
        this.userInfo = this.makeRequest({
            method: 'GET',
            path: '/userinfo',
        });
        this.token = (cred, username, password) => __awaiter(this, void 0, void 0, function* () {
            return this.makeRequest({
                method: 'POST',
                path: '/token',
                payloadKey: 'payload',
            })({
                payload: new URLSearchParams({
                    grant_type: cred.grantType,
                    client_id: cred.clientId,
                    client_secret: cred.clientSecret,
                    password: password,
                    username: username,
                }),
            });
        });
        this.refreshToken = (cred, refreshToken) => __awaiter(this, void 0, void 0, function* () {
            return this.makeRequest({
                method: 'POST',
                path: '/token',
                payloadKey: 'payload',
            })({
                payload: new URLSearchParams({
                    grant_type: cred.grantType,
                    client_id: cred.clientId,
                    client_secret: cred.clientSecret,
                    refresh_token: refreshToken,
                }),
            });
        });
    }
}
exports.OpenIdConnect = OpenIdConnect;
