import CustomKeycloakAdminClient from '../CustomKeycloakAdminClient';
import Resource from 'keycloak-admin/lib/resources/resource';
import { TCred } from '../../index';
export declare type TPermissionAccessToken = {
    access_token: string;
    expires_in: number;
    refresh_expires_in: number;
    refresh_token: string;
};
export declare type TKeycloakUserInfo = {
    sub: string;
    email_verified: boolean;
    name: string;
    preferred_username: string;
    given_name: string;
    family_name: string;
    email: string;
};
export declare class OpenIdConnect extends Resource<{
    realm?: string;
}> {
    permissionAccessToken: (cred: TCred) => Promise<TPermissionAccessToken>;
    hasPermission: ({ audience, permission }: {
        audience: string;
        permission: string;
    }) => Promise<boolean>;
    validateToken: (token: string) => Promise<boolean>;
    userInfo: (payload?: void & {
        realm?: string;
    }) => Promise<TKeycloakUserInfo>;
    token: (cred: TCred, username: string, password: string) => Promise<TPermissionAccessToken>;
    refreshToken: (cred: TCred, refreshToken: string) => Promise<TPermissionAccessToken>;
    constructor(client: CustomKeycloakAdminClient);
}
