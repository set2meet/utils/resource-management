"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Protection = void 0;
const resource_1 = __importDefault(require("keycloak-admin/lib/resources/resource"));
const index_1 = require("../../index");
class Protection extends resource_1.default {
    constructor(client) {
        super(client, {
            path: '/realms/{realm}/authz/protection',
            getUrlParams: () => ({
                realm: client.realmName,
            }),
            getBaseUrl: () => client.baseUrl,
        });
        this.createRecordingResourceDescription = (resourceName) => __awaiter(this, void 0, void 0, function* () {
            const resp = yield this.makeRequest({
                method: 'POST',
                path: '/resource_set',
            })({
                name: resourceName,
                ownerManagedAccess: true,
                resource_scopes: [index_1.Scope.READ, index_1.Scope.DELETE],
            });
            return resp._id;
        });
        this.deleteRecordingResourceDescription = this.makeRequest({
            method: 'DELETE',
            path: '/resource_set/{resourceId}',
            urlParamKeys: ['resourceId'],
        });
        this.createUmaPolicy = this.makeRequest({
            method: 'POST',
            path: '/uma-policy/{resourceId}',
            urlParamKeys: ['resourceId'],
        });
        this.getUmaPolicies = this.makeRequest({
            method: 'GET',
            path: '/uma-policy',
        });
        this.getResourceIdByResourceName = (resourceName) => __awaiter(this, void 0, void 0, function* () {
            const ids = yield this.makeRequest({
                method: 'GET',
                path: '/resource_set?name={resourceName}',
                urlParamKeys: ['resourceName'],
            })({ resourceName });
            if (ids.length !== 1) {
                throw new Error(`Cannot find resource by name ${resourceName}`);
            }
            return ids[0];
        });
    }
}
exports.Protection = Protection;
