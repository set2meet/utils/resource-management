"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const constants_1 = require("./constants");
const PREFIX_LENGTH = constants_1.RESOURCE_NAME_PREFIX.length;
const getResourceNameByGroup = (group) => group.path.substring(PREFIX_LENGTH + 1, group.path.length - group.name.length - 1);
exports.default = getResourceNameByGroup;
