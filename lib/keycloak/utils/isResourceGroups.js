"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const index_1 = require("../../index");
const isResourceGroups = (group) => group.name === index_1.GroupName.viewers || group.name === index_1.GroupName.owners;
exports.default = isResourceGroups;
