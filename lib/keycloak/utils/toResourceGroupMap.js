"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const set_1 = __importDefault(require("lodash/set"));
const getResourceNameByGroup_1 = __importDefault(require("./getResourceNameByGroup"));
const toResourceGroupMap = (resourceGroups) => {
    var _a, _b;
    const resourceGroupsCache = {};
    for (const group of resourceGroups) {
        const resourceName = getResourceNameByGroup_1.default(group);
        const pos = ((_b = (_a = resourceGroupsCache[resourceName]) === null || _a === void 0 ? void 0 : _a.groups) === null || _b === void 0 ? void 0 : _b.length) || 0;
        set_1.default(resourceGroupsCache, `${resourceName}.groups[${pos}]`, group.name);
    }
    return resourceGroupsCache;
};
exports.default = toResourceGroupMap;
