import GroupRepresentation from 'keycloak-admin/lib/defs/groupRepresentation';
declare const getResourceNameByGroup: (group: GroupRepresentation) => string;
export default getResourceNameByGroup;
