import { TResourceGroupMap } from '../../index';
import GroupRepresentation from 'keycloak-admin/lib/defs/groupRepresentation';
declare const toResourceGroupMap: (resourceGroups: GroupRepresentation[]) => TResourceGroupMap;
export default toResourceGroupMap;
