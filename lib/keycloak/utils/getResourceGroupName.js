"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const constants_1 = require("./constants");
const getResourceGroupName = (recordId) => `${constants_1.RESOURCE_NAME_PREFIX}${recordId}`;
exports.default = getResourceGroupName;
