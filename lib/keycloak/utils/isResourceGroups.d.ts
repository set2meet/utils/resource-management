import GroupRepresentation from 'keycloak-admin/lib/defs/groupRepresentation';
declare const isResourceGroups: (group: GroupRepresentation) => boolean;
export default isResourceGroups;
