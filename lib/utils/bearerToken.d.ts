declare const bearerToken: (tokenOrAuthorizationHeader: string) => string;
export default bearerToken;
