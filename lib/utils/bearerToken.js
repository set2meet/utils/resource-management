"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const BEARER_HEAD = 'bearer ';
const BEARER_LENGTH = BEARER_HEAD.length;
const bearerToken = (tokenOrAuthorizationHeader) => {
    const head = tokenOrAuthorizationHeader.substr(0, BEARER_LENGTH);
    if (head.toLowerCase().startsWith(BEARER_HEAD)) {
        return tokenOrAuthorizationHeader.substring(BEARER_LENGTH, tokenOrAuthorizationHeader.length);
    }
    return tokenOrAuthorizationHeader;
};
exports.default = bearerToken;
