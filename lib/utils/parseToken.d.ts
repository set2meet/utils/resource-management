export declare type TParsedToken = {
  baseUrl: string;
  realmName: string;
  payload: {
    iss: string;
    azp: string;
    exp: number;
    iat: number;
    sub: string;
    name: string;
  };
};
declare const parseToken: (token: string) => TParsedToken;
export default parseToken;
