"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const parseToken = (token) => {
    try {
        const decoded = jsonwebtoken_1.default.decode(token, {
            complete: true,
            json: true,
        });
        const absoluteRealmUrl = decoded.payload.iss;
        const [baseUrl, realmName] = absoluteRealmUrl.split('/realms/');
        return Object.assign({ baseUrl,
            realmName }, decoded);
    }
    catch (e) {
        throw new Error(`Cannot parse token: ${token}`);
    }
};
exports.default = parseToken;
