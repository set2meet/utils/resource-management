"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ELogErrorCode = void 0;
var ELogErrorCode;
(function (ELogErrorCode) {
    ELogErrorCode["ServiceExecution"] = "E_SERVICE_EXECUTION";
})(ELogErrorCode = exports.ELogErrorCode || (exports.ELogErrorCode = {}));
