"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getLogger = exports.registerLogger = void 0;
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
const logger_1 = require("@s2m/logger");
const ELogModule_1 = require("./ELogModule");
let cumulativeLogger;
let parentLogger = new logger_1.DummyLogger();
const initLoggers = (logger) => {
    cumulativeLogger = {
        manager: logger.createBoundChild(ELogModule_1.ELogModule.ResourceManagement),
    };
};
initLoggers(parentLogger);
exports.registerLogger = (logger) => {
    parentLogger = logger;
    initLoggers(parentLogger);
};
exports.getLogger = () => {
    return cumulativeLogger;
};
