"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.registerLogger = exports.parseToken = exports.bearerToken = exports.Scope = exports.GroupName = exports.GrantType = void 0;
const KeycloakApi_1 = __importDefault(require("./keycloak/KeycloakApi"));
const bearerToken_1 = __importDefault(require("./utils/bearerToken"));
exports.bearerToken = bearerToken_1.default;
const parseToken_1 = __importDefault(require("./utils/parseToken"));
exports.parseToken = parseToken_1.default;
const logger_1 = require("./logger/logger");
Object.defineProperty(exports, "registerLogger", { enumerable: true, get: function () { return logger_1.registerLogger; } });
var GrantType;
(function (GrantType) {
    GrantType["REFRESH_TOKEN"] = "refresh_token";
    GrantType["PASSWORD"] = "password";
    GrantType["CLIENT_CREDENTIALS"] = "client_credentials";
})(GrantType = exports.GrantType || (exports.GrantType = {}));
var GroupName;
(function (GroupName) {
    GroupName["viewers"] = "viewers";
    GroupName["owners"] = "owners";
})(GroupName = exports.GroupName || (exports.GroupName = {}));
var Scope;
(function (Scope) {
    Scope["READ"] = "read";
    Scope["DELETE"] = "delete";
})(Scope = exports.Scope || (exports.Scope = {}));
exports.default = (config) => new KeycloakApi_1.default(config);
