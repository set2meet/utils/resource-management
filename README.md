##### Config:

The library is being used for creating groups/permissions,
so you must to assign `manage-clients`, `manage-authorization`, `manage-users` roles
to client, which was created in keycloak (see config below)

params:

1. keycloak base url `baseUrl: "http://keycloak:8080/auth"`
2. keycloak realm `realmName: "Set2Meet"`
3. keycloak client grantType `grantType: "client_credentials"` (not tested with nother types)
4. keycloak client id `clientId: "Set2Meet"`
5. keycloak client secret `clientSecret: "***********************"`
