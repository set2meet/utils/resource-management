module.exports = {
  roots: ['<rootDir>/src'],
  preset: 'ts-jest',
  testEnvironment: 'node',
  testRegex: '(/__tests__/.*|(\\.|/)(test|spec))\\.tsx?$',
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
  coveragePathIgnorePatterns: ['<rootDir>/src/logger/'],
  collectCoverage: true,
  collectCoverageFrom: [
    '**/*.{js,ts}',
    '!**/node_modules/**'
  ],
  reporters: ['default', 'jest-junit'],
  coverageReporters: ['text-summary', 'cobertura', 'lcov', 'text'],
  coverageThreshold: {
    '**/*': {
      branches: 80,
      functions: 80,
      lines: 80,
      statements: -10
    }
  }
};
