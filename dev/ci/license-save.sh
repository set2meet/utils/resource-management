#!/bin/sh
if [ $# -lt 2 ]; then
	echo "license-save.sh FILE_NAME DIR"
	exit 1
fi

FILE_NAME=$1
DIR=$2
CURR_DIR=$(pwd)

/usr/bin/diff -q "$FILE_NAME" "${DIR}/${FILE_NAME}"
if [ "$?" != "0" ]; then
  cp -v "$FILE_NAME" "${DIR}/${FILE_NAME}";
  cd "$DIR";
  git add "$FILE_NAME";
  git commit -m "$FILE_NAME was added";
  git push;
  cd "$CURR_DIR";
else
  echo "$FILE_NAME not changed \n NOTHING SAVED!";
fi
